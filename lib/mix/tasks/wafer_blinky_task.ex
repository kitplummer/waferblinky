defmodule Mix.Tasks.Blink do
  use Mix.Task
  @shortdoc "GPIO LED Blink Example"

  alias Wafer.Driver.Circuits.GPIO

  def run(_args) do
    {:ok, led_pin_21} = GPIO.acquire(pin: 21, direction: :out)
    {:ok, conn} = WaferBlinky.acquire(conn: led_pin_21)

    Enum.each(1..10, fn _ ->
      WaferBlinky.turn_on(conn)
      :timer.sleep(500)
      WaferBlinky.turn_off(conn)
      :timer.sleep(500)
    end)
  end
end
