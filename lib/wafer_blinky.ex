defmodule WaferBlinky do
  @derive [Wafer.GPIO]
  defstruct ~w[conn]a
  @behaviour Wafer.Conn
  alias Wafer.Conn
  alias Wafer.GPIO

  @type t :: %WaferBlinky{conn: Conn.t()}
  @type acquire_options :: [acquire_option]
  @type acquire_option :: {:conn, Conn.t()}

  @impl Wafer.Conn
  def acquire(options) do
    with {:ok, conn} <- Keyword.fetch(options, :conn) do
      {:ok, %WaferBlinky{conn: conn}}
    else
      :error -> {:error, "`WaferBlinky.acquire/1` requires the `conn` option."}
      {:error, reason} -> {:error, reason}
    end
  end

  def turn_on(conn), do: GPIO.write(conn, 1)
  def turn_off(conn), do: GPIO.write(conn, 0)
end
